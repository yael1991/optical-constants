clear; clc;

data_re = load('eps_re_AlP_in_energies.txt');
energy = data_re(:,1);
lambda = 1240./energy;
eps_re = data_re(:,2);
lambda_interpolated = [1500:-5:180];
eps_re = interp1(lambda, eps_re,...
    lambda_interpolated,[],'extrap');
dlmwrite('eps_re_AlP.txt', [lambda_interpolated; eps_re]', 'delimiter','\t' );

data_im = load('eps_im_AlP_in_energies.txt');
energy = data_im(:,1);
lambda = 1240./energy;
eps_im = data_im(:,2);
lambda_interpolated = [1500:-5:180];
eps_im = interp1(lambda, eps_im,...
    lambda_interpolated,[],'extrap');
dlmwrite('eps_im_AlP.txt', [lambda_interpolated; eps_im]', 'delimiter','\t' );


dlmwrite('eps_AlP.txt', [lambda_interpolated; eps_re; eps_im]', 'delimiter','\t' );

eps = eps_re + 1i*eps_im;
m = sqrt(eps);
n = real(m);
k = imag(m);

figure;
hold on
plot(lambda_interpolated, n);
plot(lambda_interpolated, k);

dlmwrite('n_AlP.txt', [lambda_interpolated; n]', 'delimiter','\t' );
dlmwrite('k_AlP.txt', [lambda_interpolated; k]', 'delimiter','\t' );
dlmwrite('m_AlP.txt', [lambda_interpolated; m]', 'delimiter','\t' );