clear; clc;
addpath('~/Documents/MATLAB');
fontname = 'CMU Sans Serif';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

materials = {'BeS', 'BeSe', 'BeTe'};
color = increasing_color3(2);
for i = 1 : length(materials);
    lambda_interpolated = [150 : 1800];
    eps_re_web = load(['eps_re_', materials{i},'_web.txt']);
    eps_im_web = load(['eps_im_', materials{i},'_web.txt']);
    eps_re = interp1(1240./eps_re_web(:,1), eps_re_web(:,2), ... 
        lambda_interpolated);
    eps_im = interp1(1240./eps_im_web(:,1), eps_im_web(:,2), ... 
        lambda_interpolated);
    eps = eps_re + 1i*eps_im;
    m = sqrt(eps);
    
    dlmwrite(['eps_re_', materials{i},'.txt'], ...
        [lambda_interpolated' eps_re'], '\t');
    dlmwrite(['eps_im_', materials{i},'.txt'], ...
        [lambda_interpolated' eps_im'], '\t');
    dlmwrite(['eps_', materials{i},'.txt'], ...
        [lambda_interpolated' eps_re' eps_im'], '\t');
    
    figure; hold on;
    plot(lambda_interpolated, eps_re, 'linewidth', 3, 'color', color(1,:));
    plot(lambda_interpolated, eps_im, 'linewidth', 3, 'color', color(2,:));
    vline(400, '-k')
    set(gca, 'fontsize', 21, 'linewidth', 2); box on;
    xlabel('Wavelength (nm)'); ylabel('Dielectric function');
    legend('\epsilon_1', '\epsilon_2');
    title(materials{i}); xlim([100, 1800]);
    
    
    figure; hold on;
    plot(lambda_interpolated, real(m), 'linewidth', 3, 'color', color(1,:));
    plot(lambda_interpolated, imag(m), 'linewidth', 3, 'color', color(2,:));
    vline(400, '-k')
    set(gca, 'fontsize', 21, 'linewidth', 2); box on;
    xlabel('Wavelength (nm)'); ylabel('Refractive index');
    legend('n', 'k');
    title(materials{i}); xlim([100, 1800]);
end