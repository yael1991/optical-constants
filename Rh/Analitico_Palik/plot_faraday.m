eps_re = load('eps_re_Rh_analitico_Palik.txt');
eps_im = load('eps_im_Rh_analitico_Palik.txt');


lambda = eps_re(:,1);
eps = eps_re(:,2) + 1i*eps_im(:,2);

faraday = 9 .* abs(eps ./(eps + 2)).^2;
plot(lambda, faraday)