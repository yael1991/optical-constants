% n parte real del indice de refraccion
% k parte imaginaria del indice de refraccion
%function n_to_epsilon(nombre)
    
    nombre = 'Querry_Zr'
    n = load(['n_', nombre, '.txt']);
    k = load(['k_', nombre, '.txt']);
    
    lambda = n(:,1)*1000;
    m = n(:,2) + (1i * k(:,2));
    epsilon = (m).^2;

    eps_re = real(epsilon);
    eps_im = imag(epsilon);
    
    dlmwrite(['eps_re_',nombre,'.txt'], [lambda, eps_re], '\t')
    dlmwrite(['eps_im_',nombre,'.txt'], [lambda, eps_im], '\t')

%end