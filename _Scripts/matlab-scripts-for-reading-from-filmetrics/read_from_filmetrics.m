% Filmetrics database = http://www.filmetrics.com/refractive-index-database

MyUrl = 'https://www.filmetrics.com/technology/refractive-index-database/download/GaN'; %url de filmetrics
myFileName = 'GaN'; %nombre del elemento
urlwrite(MyUrl, myFileName);
M = importdata(myFileName);
datos = M.data;

lambda = datos(:,1);
n = datos(:,2);
k = datos(:,3);
m = n + 1i*k;
eps = m .* m;
eps_re = real(eps);
eps_im = imag(eps);


mkdir (['../',myFileName,'_filmetrics']);
cd (['../',myFileName,'_filmetrics']);
addpath('../scripts/')

% archivos de indice de refraccion
dlmwrite(['n_',myFileName,'_filmetrics.txt'], [lambda, n], '\t')
dlmwrite(['k_',myFileName,'_filmetrics.txt'], [lambda, k], '\t')
dlmwrite(['m_',myFileName,'_filmetrics.txt'], [lambda, n, k], '\t')

% archivos de permitividad
dlmwrite(['eps_re_',myFileName,'_filmetrics.txt'], [lambda, eps_re], '\t') %comsol
dlmwrite(['eps_im_',myFileName,'_filmetrics.txt'], [lambda, eps_im], '\t') %comsol
dlmwrite(['eps_',myFileName,'_filmetrics.tab'], [lambda, eps_re, eps_im], '\t') %dda