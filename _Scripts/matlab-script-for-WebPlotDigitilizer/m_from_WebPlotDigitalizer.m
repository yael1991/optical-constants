clear; clc;
fontname = 'CMU Serif';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

material = 'MoO3';
data_n = load('n_MoO3.txt');
data_k = load('k_MoO3.txt');
units = 'eV';
lambda_interpolated  =  (300:5:800);

if isequal(units, 'eV')
    lambda_n = 1240./data_n(:,1);
    n = data_n(:,2);
    lambda_k = 1240./data_k(:,1);
    k = data_k(:,2); 
    n  =  interp1(lambda_n, n,...
    lambda_interpolated,[],'extrap');
    k  =  interp1(lambda_k, k,...
    lambda_interpolated,[],'extrap');
    m = n + 1i*k;
    eps = m.*m;

elseif isequal(units, 'micras')
    lambda_n = data_n(:,1)*1000;
    n = data_n(:,2);
    lambda_k = data_k(:,1)*1000;
    k = data_k(:,2);
    n  =  interp1(lambda_n, n,...
    lambda_interpolated,[],'extrap');
    k  =  interp1(lambda_k, k,...
    lambda_interpolated,[],'extrap');
    m = n + 1i*k;
    eps = m.*m;

else
    lambda_n = data_n(:,1);
    n = data_n(:,2);
    lambda_k = data_k(:,1);
    k = data_k(:,2);
    n  =  interp1(lambda_n, n,...
    lambda_interpolated,[],'extrap');
    k  =  interp1(lambda_k, k,...
    lambda_interpolated,[],'extrap');
    m = n + 1i*k;
    eps = m.*m;
end

Figura_m = figure;
hold on
plot(lambda_interpolated, n, 'linewidth', 4)
plot(lambda_interpolated, k, 'linewidth', 4)
hold off
xlabel('wavelength (nm)')
ylabel('n, k')
legend('n', 'k')
set(gca, 'fontsize', 21, 'linewidth', 2);
box on

Figura_eps = figure;
hold on
plot(lambda_interpolated, real(eps), 'linewidth', 4)
plot(lambda_interpolated, imag(eps), 'linewidth', 4)
hold off
xlabel('wavelength (nm)')
ylabel('Dielectric Function')
legend('Re(\epsilon)', 'Im(\epsilon)')
set(gca, 'fontsize', 21, 'linewidth', 2);
box on

dlmwrite([material,'.tab'], [lambda_interpolated; real(eps); imag(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['eps_re_',material,'.txt'], [lambda_interpolated; real(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['eps_im_',material,'.txt'], [lambda_interpolated; imag(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['m_',material,'.tab'], [lambda_interpolated; n; k]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['n_',material,'.txt'], [lambda_interpolated; n]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['k_',material,'.txt'], [lambda_interpolated; k]', ...
    'delimiter','\t', 'precision','%10.5f' );