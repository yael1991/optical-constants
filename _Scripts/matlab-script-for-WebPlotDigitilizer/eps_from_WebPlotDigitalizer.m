clear; clc;
fontname = 'CMU Serif';
set(0,'defaultaxesfontname',fontname);
set(0,'defaulttextfontname',fontname);

material = 'CeO2';
data_eps_re = load('eps_re_Ag2O_web.txt');
data_eps_im = load('eps_im_Ag2O_web.txt');
units = 'eV';
lambda_interpolated  =  (150:5:1000);

if isequal(units, 'eV')
    lambda_eps_re = 1240./data_eps_re(:,1);
    eps_re = data_eps_re(:,2);
    lambda_eps_im = 1240./data_eps_im(:,1);
    eps_im = data_eps_im(:,2); 
    eps_re  =  interp1(lambda_eps_re, eps_re,...
    lambda_interpolated,[],'extrap');
    eps_im  =  interp1(lambda_eps_im, eps_im,...
    lambda_interpolated,[],'extrap');
    eps = eps_re + 1i*eps_im;
    m = sqrt(eps);
    n = real(m);
    k = imag(m);
else
    lambda_eps_re = data_eps_re(:,1);
    eps_re = data_eps_re(:,2);
    lambda_eps_im = data_eps_im(:,1);
    eps_im = data_eps_im(:,2); 
    eps_re  =  interp1(lambda_eps_re, eps_re,...
    lambda_interpolated,[],'extrap');
    eps_im  =  interp1(lambda_eps_im, eps_im,...
    lambda_interpolated,[],'extrap');
    eps = eps_re + 1i*eps_im;
    m = sqrt(eps);
end

Figura_m = figure;
hold on
plot(lambda_interpolated, n, 'linewidth', 4)
plot(lambda_interpolated, k, 'linewidth', 4)
hold off
xlabel('wavelength (nm)')
ylabel('n, k')
legend('n', 'k')
set(gca, 'fontsize', 21, 'linewidth', 2);
box on

Figura_eps = figure;
hold on
plot(1240./lambda_interpolated, real(eps), 'linewidth', 4)
plot(1240./lambda_interpolated, imag(eps), 'linewidth', 4)
hold off
xlabel('wavelength (nm)')
ylabel('Dielectric Function')
legend('Re(\epsilon)', 'Im(\epsilon)')
set(gca, 'fontsize', 21, 'linewidth', 2);
box on

dlmwrite([material,'.tab'], [lambda_interpolated; real(eps); imag(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['eps_re_',material,'.txt'], [lambda_interpolated; real(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['eps_im_',material,'.txt'], [lambda_interpolated; imag(eps)]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['m_',material,'.tab'], [lambda_interpolated; n; k]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['n_',material,'.txt'], [lambda_interpolated; n]', ...
    'delimiter','\t', 'precision','%10.5f' );
dlmwrite(['k_',material,'.txt'], [lambda_interpolated; k]', ...
    'delimiter','\t', 'precision','%10.5f' );