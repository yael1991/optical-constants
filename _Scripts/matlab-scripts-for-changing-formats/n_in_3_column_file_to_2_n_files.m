% n parte real del indice de refraccion
% k parte imaginaria del indice de refraccion
function n_in_3_column_file_to_2_n_files(nombre)

    m = load([nombre, '.txt']);
    
    lambda = m(:,1);
    n = m(:,2);
    k = m(:,3);
    
    dlmwrite(['n_',nombre,'.txt'], [lambda, n], '\t')
    dlmwrite(['k_',nombre,'.txt'], [lambda, k], '\t')

end