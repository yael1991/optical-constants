% epsRe parte real de la permitividad electrica
% epsIm imaginaria de la permitividad electrica
function eps_in_2_column_file_to_3_eps_files(nombre)

    eps_re = load(['eps_re',nombre, '.txt']);
    eps_im = load(['eps_im',nombre, '.txt']);
    
    lambda = eps_re(:,1);
    eps_re = eps_re(:,2);
    eps_im = eps_im(:,2);
    
    dlmwrite(['eps_',nombre,'.tab'], [lambda, eps_re, eps_im], '\t')

end