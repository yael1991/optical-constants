function epsilon_to_n(nombre)
    
    eps_re = load(['eps_re_', nombre, '.txt']);
    eps_im = load(['eps_im_', nombre, '.txt']);
    
    lambda = eps_re(:,1);
    epsilon = (eps_re(:,2) + (1i * eps_im(:,2)));
    m = sqrt(epsilon);

    n = real(m);
    k = imag(m);
    
    dlmwrite(['n_',nombre,'.txt'], [lambda, n], '\t')
    dlmwrite(['k_',nombre,'.txt'], [lambda, k], '\t')

end